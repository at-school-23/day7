package ru.devray.day7.equalsdemo;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Car c1 = new Car(4, "Volvo");
        Car c2 = new Car(4, "Volvo");
        Car c3 = new Car(5, "BMW");

        if (c1.hashcode() == c2.hashcode()) {
            if (c1.equals(c2)) {
                System.out.println("равны");
            } else {
                System.out.println("не равны");
            }
        } else {
            System.out.println("не равны");
        }

        System.out.println(c1);
        System.out.println(c1.equals(c2));
        System.out.println(c1.equals(c3));
        System.out.println(c1.equals(null));
        System.out.println(c1.equals("assff"));
        //toString() equals()

        List<Car> cars = new ArrayList<>();
        cars.add(c1);
        cars.add(c2);
        cars.add(c3);

        System.out.println(cars.contains(new Car(4, "Volvo")));
    }
}

package ru.devray.day7.equalsdemo;

import java.util.Objects;

public class Car {
    public int wheelCount;
    public String name;

    public Car(int wheelCount, String name) {
        this.wheelCount = wheelCount;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Car{" +
                "wheelCount=" + wheelCount +
                ", name='" + name + '\'' +
                '}';
    }

//    @Override
//    public boolean equals(Object o) {
//        if (o == null) return false;
//        Car another = (Car) o;
//        return this.name.equals(another.name) && this.wheelCount == another.wheelCount;
//    }


    //дорогостоящая операция
    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) return true;
        if (anotherObject == null || getClass() != anotherObject.getClass()) return false;
        Car car = (Car) anotherObject;
        return wheelCount == car.wheelCount && Objects.equals(name, car.name);
    }

    public int hashcode() {
        return this.wheelCount + this.name.length();
    }
}

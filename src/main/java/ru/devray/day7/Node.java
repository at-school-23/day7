package ru.devray.day7;


public class Node {
    String data;
    Node previous;
    Node next;

    public Node(String data, Node previous, Node next) {
        this.data = data;
        this.previous = previous;
        this.next = next;
    }
}

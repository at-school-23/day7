package ru.devray.day7;

import java.util.ArrayList;
import java.util.List;

public class CollectionDemo {
    public static void main(String[] args) {
        Object[] array = {1,2,3};

        //CRUD Create Read Update Delete
        List<String> list1 = new ArrayList<>(); //список на базе массива

//        List<Integer> listOfIntegers = new ArrayList<>(); //список на базе массива
        List<Integer> listOfIntegers = new ArrayList<>(); //список на базе массива
        listOfIntegers.add(23);
        listOfIntegers.add(23);
        listOfIntegers.add(23);
        listOfIntegers.add(23);

        list1.add("Jackie"); //0
        list1.add("V"); //1
        list1.add("Evelyn"); //2
        list1.add("Evelyn"); //2
        list1.add("Saburo"); //3
        list1.add("Saburo"); //3

        System.out.println(list1);
        System.out.println("Получаю доступ к 2му элементу " + list1.get(1));

        list1.add(2, "Wakako");
        list1.add(0, "4312");
        System.out.println(list1);

        list1.set(0, "Johny");
        System.out.println(list1);

        list1.remove(3);
        System.out.println(list1);

        list1.remove("Saburo");
        System.out.println(list1);

        boolean hasTonyElement = list1.contains("Tony");
        System.out.println(hasTonyElement);
        System.out.println(list1.contains("Evelyn"));

        System.out.println(list1.size());
        System.out.println(array.length);

    }
}

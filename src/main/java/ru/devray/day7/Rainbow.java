package ru.devray.day7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class Rainbow {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("красный");
        list.add("оранжевый");
        list.add("желтый");
        list.add("зеленый");
        list.add("голубой");
        list.add("синий");
        list.add("фиолетовый");

        System.out.println(list.size());
        String s = list.get(3);


        //меняем местами оранжевый с зеленым

//        String color2 = list.get(1);
//        list.set(1, list.get(3));
//        list.set(3, color2);

        Collections.swap(list,1,3);

//        System.out.println(list);

        list.remove("фиолетовый");


//        for (int i = 0; i < 10; i++) {
//            System.out.println(list.get(i));
//        }



        for (String color : list) { //
            System.out.println(color);
        }

        Iterator<String> iterator = list.iterator();
        while(iterator.hasNext()) {
            String color = iterator.next();
            System.out.println(color);
        }

        System.out.println(Rainbow.class);
        System.out.println(s.getClass());

        //Iterable - перебираемый



    }

}
